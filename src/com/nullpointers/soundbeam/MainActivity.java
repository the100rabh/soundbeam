package com.nullpointers.soundbeam;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;

public class MainActivity extends Activity {

	public static long startTime = 0;
	public static long inc = 30;
	public static List<String> outVal = new ArrayList<String>();
	public static List<short[]> bufferData = new ArrayList<short[]>();
	public static long lastTime = 0;
	final AudioReader audioReader = new AudioReader();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		int sampleRate = 44100;
		int inputBlockSize = 64;
		int sampleDecimate = 2;

		// AudioGenerator generator = new AudioGenerator();
		// generator.genTone(6000, 5);

		audioReader.startReader(sampleRate, inputBlockSize * sampleDecimate,
				new AudioReader.Listener() {
					@Override
					public final void onReadComplete(short[] buffer) {
						long temp = System.currentTimeMillis();

						if (startTime != 0) {
							if (temp >= (startTime + 470)) {
								startTime += 500;
								Log.e("StartTIme", "" + temp);
							} else {
								// Log.e("StartTIme", "2");
								return;
							}
						} else {
							
							if(lastTime == 0){
								lastTime = temp;
							}
							else if(lastTime + 200 > temp){
								return;
							}
							lastTime = temp;
							receiveAudio(buffer);
						}
						synchronized (bufferData) {

							bufferData.add(buffer);
							
						}
						handler.post(new Runnable() {

							@Override
							public void run() {
								short[] buffer;
								synchronized (bufferData) {
									buffer = bufferData.get(0);
									bufferData.remove(0);
								}
								receiveAudio(buffer);
							}
						});
						// receiveAudio(buffer);
					}

					protected void handleError(int error) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onReadError(int error) {
						handleError(error);
					}
				});

	}

	protected void receiveAudio(short[] buffer) {
		long temp = System.currentTimeMillis();
		String outString = "";
		int iCount = 0;
		int datacount = buffer.length;

		Complex[] x = new Complex[datacount];

		// original data
		for (int i = 0; i < datacount; i++) {
			x[i] = new Complex(buffer[i], 0);
		}

		// FFT of original data
		Complex[] y = FFT.fft(x);
		String out = "y = ";

		double total = 0;
		for (iCount = 50; iCount < 64; iCount++) {
			total += y[iCount].abs();
		}
		double avg = total / 14;
		boolean is53up = false, is55up = false, is58up = false;
		for (iCount = 50; iCount < 64; iCount++) {
			if (avg + 5000 < y[iCount].abs()) {
				if (iCount == 51 || iCount == 52) {
					is53up = true;
					Log.e("dta", "53 up");
				} else if (iCount == 55 || iCount == 54) {
					is55up = true;
					Log.e("dta", "55 up");
				} else if (iCount == 58 || iCount == 57) {
					is58up = true;
					Log.e("dta", "58 up");
				}
			}
		}

		if (is53up && startTime == 0) {
			startTime = temp ;//+ 500;
			Log.e("StartTIme", "" + temp);
			Log.e("Got", "start");
		} else if (is53up && is55up) {
			outVal.add(".");
			Log.e("Got", "dot");
		} else if (is53up && is58up) {
			outVal.add("-");
			Log.e("Got", "dash");
		} else if (is53up) {
			outVal.add(" ");
			Log.e("Got", "space");
		} else if (startTime != 0) {
			Log.e("StartTIme", "" + temp);
			Log.e("Got", "End reached");
			startTime = 0;
			String outStrings = "string data";
			for (String strV : outVal) {
				outStrings = outStrings + strV;
				// Log.e("Got", strV);
			}
			Log.e("Got", outStrings);
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					String outStrings = "Recieved Data";
					for (String strV : outVal) {
						outStrings = outStrings + strV;
					}
					outVal.clear();
					((TextView) (findViewById(R.id.textView1)))
							.setText(outStrings);

				}
			});
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		audioReader.stopReader();
	}

	class ReceiverAsycTask extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
		}

	}

	Handler handler = new Handler();

	@Override
	protected void onResume() {
		super.onResume();

		// Use a new tread as this can take a while
		Thread thread = new Thread(new Runnable() {
			public void run() {
				AudioGenerator.genTone();
				handler.post(new Runnable() {

					public void run() {
						AudioGenerator.playSound();
						Log.e("Generator", "Sound being played");
					}
				});
			}
		});
		thread.start();
	}

}
